#ifndef __TASK_H__
#define __TASK_H__

typedef struct 
{
	char *name;
	void (*entry)(void *parameter);
	void *parameter;
	rt_uint32_t stack_size;
	rt_uint8_t  priority;
	rt_uint32_t tick;
}TaskStruct;

void Task_Create(void);
rt_thread_t beep_task_create(void);

#endif /*__TASK_H__ */
