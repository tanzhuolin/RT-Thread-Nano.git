#include "config.h"
#include "system.h"

void System_Init(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);/* 设置中断优先级分组 */
	
	/*********************************************************************/
	//外设驱动初始化
	SysTick_Init();
	FSMC_SRAM_Init();
	LED_Config();
	COLOR_TIMx_LED_Init();
	RTC_Configuration();
	USART_Config();
	EXTI_Key_Config();
	ADC_Config();
	Esp8266_Config();
}
