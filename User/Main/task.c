#include "config.h"
#include "task.h"

/*************************************************************************
*                            变量定义
**************************************************************************
*/
static rt_thread_t dynamic_thread = RT_NULL;

/*************************************************************************
*                            线程入口函数声明
**************************************************************************
*/
void iwdg_thread_entry(void *parameter);
void led_thread_entry(void *parameter);
void usart1_recv_thread_entry(void *parameter);
void mp3_thread_entry(void *parameter);
void adc_detect_thread_entry(void *parameter);
void esp8266_thread_entry(void *parameter);
void lcd_thread_entry(void *parameter);
//void ntp_thread_entry(void *parameter);
//void weather_thread_entry(void *parameter);

TaskStruct TaskThreads[] = {
		
		//{"iwdg_thread", iwdg_thread_entry, RT_NULL, 512, 15, 10},
		{"led_thread", led_thread_entry, RT_NULL, 256, 15, 10},
		{"usart1_recv_thread", usart1_recv_thread_entry, RT_NULL, 512, 2, 10},
		//{"ntp_thread", ntp_thread_entry, RT_NULL, 512, 20, 10},
		//{"weather_thread", weather_thread_entry, RT_NULL, 512, 20, 10},
		{"lcd_thread", lcd_thread_entry, RT_NULL, 2048, 12, 10},
		{"mp3_thread", mp3_thread_entry, RT_NULL, 2048, 2, 10},
		//{"adc_detect_thread", adc_detect_thread_entry, RT_NULL, 512, 5, 10},	
		
		/*********************************************************/
		//用户添加线程参数
		//例如：{线程名字,线程入口函数,线程入口函数参数,线程栈大小,线程的优先级,线程时间片},
		
		{" ", RT_NULL, RT_NULL, RT_NULL, RT_NULL, RT_NULL},

};


void Task_Create(void)
{
	uint8_t TaskThreadsindex = 0;
	
	while(1)
	{
		if( strcmp(TaskThreads[TaskThreadsindex].name, " ") != 0 )
		{
			dynamic_thread =  rt_thread_create(TaskThreads[TaskThreadsindex].name,				/* 创建LED线程 */
																				 TaskThreads[TaskThreadsindex].entry,				/* 线程入口函数 */
																				 TaskThreads[TaskThreadsindex].parameter,		/* 线程参数 */
																				 TaskThreads[TaskThreadsindex].stack_size,	/* 线程栈大小 */
																				 TaskThreads[TaskThreadsindex].priority,		/* 线程优先级 */
																				 TaskThreads[TaskThreadsindex].tick);				/* 线程时间片 */
			if( dynamic_thread != RT_NULL )
			{
				rt_kprintf("%s success!\n", TaskThreads[TaskThreadsindex].name);
				rt_thread_startup(dynamic_thread);
			}
			else
			{
				rt_kprintf("%s fail!\n", TaskThreads[TaskThreadsindex].name);
			}
			
			dynamic_thread = RT_NULL;
			TaskThreadsindex ++;
		}
		else
			break;
	}
}

void iwdg_thread_entry(void *parameter)
{
	IWDG_Config(IWDG_Prescaler_64 ,1250);  	/* IWDG 2s 超时溢出 */
	
	while(1)
	{
		IWDG_Feed();
		rt_thread_mdelay(1000);								/* 每隔 1s 进行喂狗操作 */
	}
}

void led_thread_entry(void *parameter)
{
	while(1)
	{
		//LED_Toggle(GPIOF, GPIO_Pin_7);
		rt_thread_mdelay(2000);
	}
}

static u8 GetLevel(char *str)
{
	char buf[24];
	char i;
	for( i='0'; i<='9'; i++ )
	{
		sprintf(buf, "%s%s", "Brightness", &i);
		if( strstr(str, buf) )
			return (i-'0');
	}
	
	return 0;
}


void led_timer_timeout(void *parameter)
{
	if( *(u8 *)parameter == 1 )
		LED_Toggle(GPIOF, GPIO_Pin_7);
	else if( *(u8 *)parameter == 2 )
		LED_Toggle(GPIOF, GPIO_Pin_8);
}

static rt_sem_t music_sem;
void mp3_thread_entry(void *parameter)
{
	rt_err_t res;
	
	while(1)
	{
		res = rt_sem_take(music_sem, RT_WAITING_FOREVER);
		
		if( res == RT_EOK )
		{
			vs1053_player_song("0:/music/TestFile.mp3");	
		}
		rt_thread_mdelay(1000);
	}
}



void usart1_recv_thread_entry(void *parameter)
{
	uint8_t *buffer = RT_NULL;
	uint8_t data;
	uint16_t index = 0;
	rt_err_t res;
	u8 led_level;
	rt_timer_t led_timer;
	u8 led_num;

	music_sem = rt_sem_create("music_sem", 0, RT_IPC_FLAG_FIFO);
	if( music_sem != RT_NULL )		
		rt_kprintf("music_sem success!\n");		
	else		
		rt_kprintf("music_sem fail!\n");		
	
	usart1_recv_msg_mq = rt_mq_create("usart1_recv_msg_mq",				/* 创建消息队列用于串口1数据接收 */
																		 1,													/* 消息队列最大长度 byte */
																		 256,												/* 消息的数量 */
																		 RT_IPC_FLAG_FIFO);					/* 如果有多个线程等待此消息队列，采用先进先出的方式进行线程切换 */
	if( usart1_recv_msg_mq != RT_NULL )		
		rt_kprintf("usart1_recv_msg_mq success!\n");		
	else		
		rt_kprintf("usart1_recv_msg_mq fail!\n");		
	
	led_timer = rt_timer_create("led_timer",											//定时器名字
																led_timer_timeout,       				//定时器回调函数
																(void *)&led_num,              	//参数
																1000,    												//1s检测一次按键
																RT_TIMER_FLAG_PERIODIC | 				//周期调用
																RT_TIMER_FLAG_SOFT_TIMER );			//软件定时
	
	buffer = rt_malloc(256);																			/* 分配一个256字节的串口接收缓冲区 */

	if( buffer == RT_NULL )
	{
		rt_kprintf("buffer fail!\n");
		return ;
	}
	
	while(1)
	{
		res = rt_mq_recv(usart1_recv_msg_mq, (void*)&data, 1, 40);	/* 从消息队列中接收消息到 data */
		
    if( RT_EOK == res )																					/* 有数据过来，将数据进行保存 */
    {
			index = index >= 254 ? 254 : index;
			buffer[index++] = data;
    }
		else if( -RT_ETIMEOUT == res )															/* 超出40ms没有接收到数据，说明串口空闲了 */
		{
			if( index > 0 )
			{
				buffer[index] = '\0';
			
				rt_kprintf("Usart1 Recv Buffer : %s \n", buffer);
				
				if( strstr((char *)buffer, "Toggle") && strstr((char *)buffer, "LED1") )
				{
					rt_timer_stop(led_timer);
					led_num = 1;
					rt_timer_start(led_timer);
				}
				else if( strstr((char *)buffer, "Toggle") && strstr((char *)buffer, "LED2") )
				{
					rt_timer_stop(led_timer);
					led_num = 2;
					rt_timer_start(led_timer);
				}
				else if( strstr((char *)buffer, "Close") && strstr((char *)buffer, "LED") )
				{
					rt_timer_stop(led_timer);
					GPIO_SetBits(GPIOF, GPIO_Pin_7);
					GPIO_SetBits(GPIOF, GPIO_Pin_8);
				}
				else if( strstr((char *)buffer, "BeepOn") )
				{
					BeepOn();
				}
				else if( strstr((char *)buffer, "BeepOff") )
				{
					BeepOff();
				}
				else if( strstr((char *)buffer, "Music") )
				{
					rt_sem_release(music_sem);
					//vs1053_player_song("0:/music/TestFile.mp3");	
				}
				else
				{
					led_level = 0;
					led_level = GetLevel((char *)buffer);
					
					if( strstr((char *)buffer, "LEDR") )
					{
						SetColorValue(20*led_level, 0, 0);
					}	
					else if( strstr((char *)buffer, "LEDG" ) )
					{
						SetColorValue(0, 20*led_level, 0);
					}
					else if( strstr((char *)buffer, "LEDB" ) )
					{
						SetColorValue(0, 0, 20*led_level);
					}	
				}
				rt_memset(buffer, 0, 256);/* 清空缓冲区 */
				index = 0;
			}
		}
		else if( -RT_ERROR == res )
		{
			rt_kprintf("usart1_recv_msg_mq res error!");
		}
	}	
}

void adc_detect_thread_entry(void *parameter)
{
	rt_err_t res;
	rt_uint32_t e;
	rt_timer_t adc_timer;
	
	adc_timer = rt_timer_create("adc_timer",
															 adc_value_update,
															 RT_NULL,
															 1000,
															 RT_TIMER_FLAG_PERIODIC);
	
	if( adc_timer != RT_NULL )
		rt_timer_start(adc_timer);
	else
		rt_kprintf("adc_timer success!\n");
	
	adc_alarm_event = rt_event_create("adc_alarm_event", RT_IPC_FLAG_FIFO);
	
	if( adc_alarm_event == RT_NULL )
		rt_kprintf("adc_alarm_event fail!\n");
	
	while(1)
	{
		res = rt_event_recv(adc_alarm_event,
                       EVENT_FLAG1 | EVENT_FLAG3,
                       RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,
                       RT_WAITING_FOREVER,
											 &e);
		
		if( res == RT_EOK )
		{
			if( e & EVENT_FLAG1 )
				rt_kprintf("warnning PC1 < 0.5V\n");
			
			if( e & EVENT_FLAG3 )
				rt_kprintf("warnning PC3 > 3.0V\n");
		}
		else
		{
			rt_kprintf("adc_alarm_event res error!\n");
		}
	}
}

static void beep_thread_entry(void *parameter)
{
	while(1)
	{
		//BeepOn();
		rt_thread_mdelay(500);
		//BeepOff();
		rt_thread_mdelay(500);
	}
}

rt_thread_t beep_task_create(void)
{
	rt_thread_t tid;

	tid = rt_thread_create("beep_task",
													beep_thread_entry, RT_NULL,
													512, 20, 10);

	if (tid != NULL)
		rt_thread_startup(tid);
	
	return tid;
}


extern s_myclock tan_clock;
static FATFS fs;

void lcd_thread_entry(void *parameter)
{
	uint32_t local_timestamp; 
  struct tm * Net_time;
	u8 i;

	f_mount(&fs,"0:",1);
	VS_Init();
	rt_thread_mdelay(100);
	VS_HD_Reset();
	VS_Soft_Reset();
	
	GetNtp();
	
	while(1)
	{
//		vs1053_player_song("0:/music/TestFile.mp3.mp3");
		local_timestamp = Get_Time();
		Net_time = localtime(&local_timestamp);
		if( Net_time->tm_hour == tan_clock.getup_hour && Net_time->tm_min == tan_clock.getup_min && Net_time->tm_sec == 0 )
		{
			for(i=0; i<4; i++)
			{	
				vs1053_player_song("0:/music/morning.mp3");
				rt_thread_mdelay(1000);
			}
		}
		else if( Net_time->tm_hour == tan_clock.goout_hour && Net_time->tm_min == tan_clock.goout_min && Net_time->tm_sec == 0 )
		{
			for(i=0; i<4; i++)
			{
				WeatherForecast();
				rt_thread_mdelay(1000);
				vs1053_player_song("0:/music/litter.mp3");
				rt_thread_mdelay(1000);
			}
		}
		else if( Net_time->tm_hour == tan_clock.sport_hour && Net_time->tm_min == tan_clock.sport_min && Net_time->tm_sec == 0 )
		{
			for(i=0; i<4; i++)
			{	
				vs1053_player_song("0:/music/sport.mp3");
				rt_thread_mdelay(1000);
			}
		}		
		else if( Net_time->tm_hour == tan_clock.sleep_hour && Net_time->tm_min == tan_clock.sleep_min && Net_time->tm_sec == 0 )
		{
			for(i=0; i<4; i++)
			{	
				vs1053_player_song("0:/music/sleep.mp3");
				rt_thread_mdelay(1000);
			}
		}
		else if( Net_time->tm_hour == tan_clock.calorifier_hour && Net_time->tm_min == tan_clock.calorifier_min && Net_time->tm_sec == 0 )
		{
			BeepOn();
		}		
		rt_thread_mdelay(1000);
	}
}

