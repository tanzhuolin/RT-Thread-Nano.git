#ifndef __CONFIG_H__
#define __CONFIG_H__

//头文件被多个C调用时，避免变量定义冲突问题
#ifdef MAIN_CONFIG
	#define EXT 
#else
	#define EXT extern
#endif

//--------------------------------------------
typedef enum{
	
	/* 用户自自定义消息类型，例如MSG_XXX */
	MSG_NULL = 0,
	
	/* 按键消息类型 */
	MSG_KEY1_PRESS,
	MSG_KEY2_PRESS,
	
	/* 消息类型总数 */
	MSG_NUM
	
}MSG_TYPE;


//--------------------------------------------
#define EVENT_FLAG1 	(1<<1)		//ADC通道1小于0.5V事件
#define EVENT_FLAG3 	(1<<3)		//ADC通道3大于3.3V事件


//---------------------------------------------
//RTOS相关头文件包含
#include <rthw.h>
#include <rtthread.h>



//---------------------------------------------
//常用头文件包含
#include "stm32f10x.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "time.h"
#include <stdlib.h>

//---------------------------------------------
//外设驱动有文件包含
#include "system.h"
#include "bsp_SysTick.h"
#include "bsp_gpio.h"  
#include "bsp_usart.h"
#include "bsp_exti.h"
#include "bsp_timer.h"
#include "bsp_iwdg.h"   
#include "bsp_button.h"
#include "bsp_adc.h"
#include "bsp_esp8266.h"
#include "bsp_rtc.h"
#include "bsp_color_led.h"
#include "Task.h"

//---------------------------------------------
//MP3和文件系统头文件包含
#include  "VS1053.h"
#include "ff.h"
#include "fonts.h"
#include "bsp_ili9341_lcd.h"	  
#include "ntp_example.h"
#include "./sram/sram.h"	

//---------------------------------------------
//消息队列结构体
struct msg
{
	rt_uint8_t *data_ptr;    	/* 消息首地址 */
  rt_uint16_t *data_size;   /* 消息大小   */
};

//---------------------------------------------
//全局变量定义，请使用EXT修饰
EXT rt_mq_t usart1_recv_msg_mq;		/* 定义串口1接收消息队列控制块指针 */
EXT rt_event_t adc_alarm_event;		/* ADC报警事件 */
EXT rt_mq_t led_msg_mq;						/* 定义LED闪烁消息队列控制块指针 */
EXT rt_mq_t clock_msg_mq;

#endif /* __CONFIG_H__ */

/********************************************************************************************************
**                            End Of File
********************************************************************************************************/
