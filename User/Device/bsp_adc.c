#include "config.h"
#include "bsp_adc.h"

__IO uint16_t ADC_ConvertedValue[NOFCHANEL]= {0,0};

static void ADC_GPIO_Config(void)
{
	GPIO_InitTypeDef g;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	g.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_3;
	g.GPIO_Mode = GPIO_Mode_AIN;
	
	GPIO_Init(GPIOC, &g);
}

static void ADC_Mode_Config(void)
{
	DMA_InitTypeDef d;
	ADC_InitTypeDef a;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	/* ADC-DMA初始化 */
	DMA_DeInit(DMA1_Channel1);
	
	d.DMA_PeripheralBaseAddr = (uint32_t)(&(ADC1->DR));
  d.DMA_MemoryBaseAddr = (uint32_t)ADC_ConvertedValue;
  d.DMA_DIR = DMA_DIR_PeripheralSRC;
	d.DMA_BufferSize = NOFCHANEL;
  d.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  d.DMA_MemoryInc = DMA_MemoryInc_Enable;
	d.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  d.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  d.DMA_Mode = DMA_Mode_Circular;
	d.DMA_Priority = DMA_Priority_High;
	d.DMA_M2M = DMA_M2M_Disable;
	
	DMA_Init(DMA1_Channel1, &d);
	DMA_Cmd(DMA1_Channel1, ENABLE);
	
	/* ADC初始化 */
	ADC_DeInit(ADC1);
	a.ADC_Mode = ADC_Mode_Independent;
	a.ADC_ScanConvMode = ENABLE;
	a.ADC_ContinuousConvMode = ENABLE;
	a.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	a.ADC_DataAlign = ADC_DataAlign_Right;
	a.ADC_NbrOfChannel = NOFCHANEL;
	ADC_Init(ADC1, &a);
	
	/* 配置ADC时钟PCLK2的8分频，即9MHz */
	RCC_ADCCLKConfig(RCC_PCLK2_Div8); 
	
	/* 配置ADC 通道的转换顺序和采样时间 */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 1, ADC_SampleTime_55Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 2, ADC_SampleTime_55Cycles5);
	
	/* 使能ADC的DMA请求，打开ADC */
	ADC_DMACmd(ADC1, ENABLE);
	ADC_Cmd(ADC1, ENABLE);
	
	ADC_ResetCalibration(ADC1);									// 初始化ADC 校准寄存器  
	while(ADC_GetResetCalibrationStatus(ADC1));	// 等待校准寄存器初始化完成
	
	ADC_StartCalibration(ADC1);									// ADC开始校准
	while(ADC_GetCalibrationStatus(ADC1));			// 等待校准完成
	
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);			// 由于没有采用外部触发，所以使用软件触发ADC转换 
}

void ADC_Config(void)
{
	ADC_GPIO_Config();
	ADC_Mode_Config();
}

void adc_value_update(void *parameter)
{
	float ADC_ConvertedValueLocal[NOFCHANEL];
	
	rt_interrupt_enter();
	
	ADC_ConvertedValueLocal[0] = (float)ADC_ConvertedValue[0]/4096*3.3;
	ADC_ConvertedValueLocal[1] = (float)ADC_ConvertedValue[1]/4096*3.3;
	
	rt_interrupt_leave();
	
//	rt_kprintf("PC1 = %d \n", ADC_ConvertedValue[0]);
//	rt_kprintf("PC3 = %d \n", ADC_ConvertedValue[1]);
	
	if( ADC_ConvertedValueLocal[0] < 0.5 )//PC1电位器小于0.5V
		rt_event_send(adc_alarm_event, EVENT_FLAG1);
	
	if( ADC_ConvertedValueLocal[1] > 3.0 )//PC3大于3.0V
		rt_event_send(adc_alarm_event, EVENT_FLAG3);
}

