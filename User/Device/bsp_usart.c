
//--------------------------------------------------
//头文件相关
#include "config.h"
#include "bsp_usart.h"

#define DEBUG_USARTx 	USART2

void USART_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

#if USART1_EN == 1	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	//GPIO USART1-Tx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//GPIO USART1-Rx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//USART1-Config
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = 
	USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);

	//NVIC-Config
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	//INT-Select
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);//添加串口空闲中断使能，请不要使用USART_IT_RXNE|USART_IT_IDLE，记住分开写两条语句

	USART_Cmd(USART1, ENABLE);
	
	USART_ClearFlag(USART1, USART_FLAG_TC);//清发送完成标志位
#endif

#if USART2_EN == 1
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	//GPIO USART2-Tx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//GPIO USART2-Rx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//USART2-Config
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(DEBUG_USARTx, &USART_InitStructure);


	USART_Cmd(DEBUG_USARTx, ENABLE); 
	
	USART_ClearFlag(DEBUG_USARTx, USART_FLAG_TC);//清发送完成标志位
#endif
}

/* 重定向c库函数printf到串口2，重定向后可使用printf函数 */
int fputc(int ch, FILE *f)
{
		/* 发送一个字节数据到串口 */
		USART_SendData(USART2, (uint8_t) ch);
		
		/* 等待发送完毕 */
		while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);		
	
		return (ch);
}

/* 实现该函数以后，才能使用rt_kprintf */
void rt_hw_console_output(const char *str)
{
	rt_enter_critical();/* 进入临界段 */
	
	while( *str != '\0' )
	{
		if( *str == '\n' )
		{
			USART_SendData(DEBUG_USARTx, '\r');
			while (USART_GetFlagStatus(DEBUG_USARTx, USART_FLAG_TC) == RESET);	
		}
		
		USART_SendData(DEBUG_USARTx, *str++);
		while (USART_GetFlagStatus(DEBUG_USARTx, USART_FLAG_TC) == RESET);	
	}
	
	rt_exit_critical();/* 退出临界段 */
}

/* 实现该函数以后，才能使用Finsh */
char rt_hw_console_getchar(void)
{
	int ch = -1;

	if( USART_GetFlagStatus(DEBUG_USARTx, USART_FLAG_RXNE) != RESET )
	{
		ch = (int)USART_ReceiveData(DEBUG_USARTx);
		USART_ClearFlag(DEBUG_USARTx, USART_FLAG_RXNE);
	}
	else
	{
		if( USART_GetFlagStatus(DEBUG_USARTx, USART_FLAG_ORE) != RESET )
		{
			USART_ClearFlag(DEBUG_USARTx, USART_FLAG_ORE);
		}
		rt_thread_mdelay(10);
	}
	return ch;
}

#if 0 /* 这种接收方式并不是太好，因为有可能数据发送太快而丢包 */
static void send_op(void *data, rt_uint16_t *length)
{    
	struct msg  msg_ptr;

	msg_ptr.data_ptr = data;  	/* 指向相应的数据块地址 */
	msg_ptr.data_size = length; /* 数据块的长度 */

	/* 发送这个消息指针给 mq 消息队列 */
	rt_mq_send(usart2_recv_msg_mq, (void*)&msg_ptr, sizeof(struct msg));
}

/* 串口2数据接收，利用接收中断和空闲中断 */
#if USART2_EN == 1
void USART2_IRQHandler(void)
{
	u8 data;
	
	if( USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == SET )
	{
		data = (uint8_t)USART_ReceiveData(USART2);
		
		if( g_USART2_Recv_Pos < USART2_RECV_MAX_LEN - 1 )/* 0-254可以存进来，255用来存字符串结束符 */
			g_USART2_Recv_Buffer[g_USART2_Recv_Pos++] = data;
		
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
	}
	
	if( USART_GetFlagStatus(USART2, USART_FLAG_ORE) == SET )
	{
		USART_ClearFlag(USART2, USART_FLAG_ORE);
	}
	
	if( USART_GetFlagStatus(USART2, USART_FLAG_IDLE) == SET )
	{
		data = (uint8_t)USART_ReceiveData(USART2);/* 清除空闲中断的时候，不能用USART_ClearFlag函数，而是再读取一次 */
		g_USART2_Recv_Buffer[g_USART2_Recv_Pos++] = '\0';
		
		send_op(g_USART2_Recv_Buffer, &g_USART2_Recv_Pos); /* 发送消息*/
	}
}
#endif
#endif 

#if USART1_EN == 1
void USART1_IRQHandler(void)
{
	u8 data;
	
	if( USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET )
	{
		data = (uint8_t)USART_ReceiveData(USART1);
		
		rt_mq_send(usart1_recv_msg_mq, (void*)&data, 1);/* 将接收到的数据发送线程 */
		USART_ClearFlag(USART1, USART_FLAG_RXNE);
	}
	
	if( USART_GetFlagStatus(USART1, USART_FLAG_ORE) == SET )
	{
		USART_ClearFlag(USART1, USART_FLAG_ORE);
	}
}
#endif

