#if 0
#include "config.h"
#include "bsp_timer.h"

void Tim2_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	
	TIM_TimeBaseInitStruct.TIM_Prescaler = 71;/* 71分频 72MHz / 72 = 1MHz 1us */												 
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;     													 
	TIM_TimeBaseInitStruct.TIM_Period = 1000;/* 1us * 1000 = 1ms */
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;

	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct);
	
	//NVIC-Config
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);					//清除溢出中断标志
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);		//开启TIM2的更新中断
	TIM_Cmd(TIM2, ENABLE);
}

void TIM2_IRQHandler(void)
{
	if( TIM_GetITStatus(TIM2, TIM_FLAG_Update) == SET )
	{
		//***************************************
		//添加用户代码
		ButtonProj();
		
		TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);
	}
}
#endif 
