#ifndef __USART_H
#define	__USART_H

#include "stm32f10x.h"

//--------------------------------------------------
//串口选择
#define USART1_EN 1
#define USART2_EN 1

//--------------------------------------------------
//串口1-接收缓冲区声明
//#if USART1_EN == 1
//#define USART1_RECV_MAX_LEN 	256
//extern uint8_t g_USART1_Recv_Buffer[USART1_RECV_MAX_LEN];
//extern uint16_t g_USART1_Recv_Pos;
//#endif 

void USART_Config(void);
void USART_SendByte(USART_TypeDef* USARTx, uint8_t data);

#endif /* __USART_H */
