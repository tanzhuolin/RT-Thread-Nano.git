#include "config.h"
#include "bsp_gpio.h"

//-------------------------------------------------------------
//LED ��ʼ��
void LED_Config(void)
{		
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd( LED1_GPIO_CLK | LED2_GPIO_CLK | LED3_GPIO_CLK, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED1_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	GPIO_Init(LED1_GPIO_PORT, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = LED2_GPIO_PIN;
	GPIO_Init(LED2_GPIO_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = LED3_GPIO_PIN;
	GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStructure);

	GPIO_SetBits(LED1_GPIO_PORT, LED1_GPIO_PIN);
	GPIO_SetBits(LED2_GPIO_PORT, LED2_GPIO_PIN);
	GPIO_SetBits(LED3_GPIO_PORT, LED3_GPIO_PIN);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
	
	//PF7 D4 Blue LED
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_Init(GPIOF, &GPIO_InitStructure);
	GPIO_SetBits(GPIOF, GPIO_Pin_7);
	
	//PF8 D5 Blue LED
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_Init(GPIOF, &GPIO_InitStructure);
	GPIO_SetBits(GPIOF, GPIO_Pin_8);
}

void LED_Toggle(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	GPIOx->ODR ^= GPIO_Pin;
}


//-------------------------------------------------------------
//BEEP ��ʼ�� PC0 
int Beep_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	GPIO_ResetBits(GPIOC, GPIO_Pin_0);
	
	return RT_EOK;
}
INIT_BOARD_EXPORT(Beep_Config);

void BeepOn(void)
{
	GPIO_SetBits(GPIOC, GPIO_Pin_0);
}
MSH_CMD_EXPORT(BeepOn, Beep On);


void BeepOff(void)
{
	GPIO_ResetBits(GPIOC, GPIO_Pin_0);
}
MSH_CMD_EXPORT(BeepOff, Beep Off);


/*********************************************END OF FILE**********************/
