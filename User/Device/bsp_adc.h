#ifndef __ADC_H
#define __ADC_H

#include "stm32f10x.h"

// 转换通道个数
#define    NOFCHANEL		2

extern __IO uint16_t ADC_ConvertedValue[NOFCHANEL];

void ADC_Config(void);
void adc_value_update(void *parameter);

#endif /* __ADC_H */
