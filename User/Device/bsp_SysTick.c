#include "config.h"
#include "bsp_SysTick.h"

void SysTick_Init(void)
{
	if (SysTick_Config(SystemCoreClock / RT_TICK_PER_SECOND))
	{ 
		/* Capture error */ 
		while (1);
	}
}

/*********************************************END OF FILE**********************/
