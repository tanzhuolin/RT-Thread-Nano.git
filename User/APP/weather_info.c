#include "config.h"
#include "weather_info.h"
#include "CJSON.h"

/* 心知天气（api.seniverse.com）--天气数据的来源 */
#define WEATHER_IP 		"116.62.81.138"
#define WEATHER_PORT 	"80"

/* 定义一些全局变量 */
//------------------------------------------------------------------------------------------
//所需要的天气数据,供其他应用使用
WeatherType weather_data;


/********************************************************************************************************
** 函数: cJSON_NowWeatherParse, 解析天气实况数据包now.json
**------------------------------------------------------------------------------------------------------
** 参数: JSON:天气实况数据包  WeatherType:存储解析得到的有用的数据
** 说明: 数据来源：心知天气（api.seniverse.com）
** 返回: 0:解析成功 其他：解析失败		
** 作者: 2017.12.6 by Hezhijie and Lizhengnian 
********************************************************************************************************/

static int cJSON_NowWeatherParse(char *JSON, WeatherType *result)
{

	cJSON *json,*arrayItem,*object,*subobject,*item;

	
//printf("Receive now data:%s\r\n",JSON);
 
	json = cJSON_Parse(JSON);//解析JSON数据包
	if(json == NULL)		  //检测JSON数据包是否存在语法上的错误，返回NULL表示数据包无效
	{
		printf("Error before: [%s]\r\n",cJSON_GetErrorPtr()); //打印数据包语法错误的位置
		return 1;
	}
	else
	{
		if((arrayItem = cJSON_GetObjectItem(json,"results")) != NULL); //匹配字符串"results",获取数组内容
		{
			int size = cJSON_GetArraySize(arrayItem);     //获取数组中对象个数
//			printf("cJSON_GetArraySize: size=%d\n",size); 
			
			if((object = cJSON_GetArrayItem(arrayItem,0)) != NULL)//获取父对象内容
			{
        printf("--------------------------------Now Weather Data Begin------------------------------\r\n");
				/* 匹配子对象1 */
				if((subobject = cJSON_GetObjectItem(object,"location")) != NULL)
				{
					
					printf("---------------------------------subobject1-------------------------------\r\n");
					if((item = cJSON_GetObjectItem(subobject,"id")) != NULL)   
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"name")) != NULL) //地名--需要用到的数据
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"country")) != NULL)
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"path")) != NULL)  
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"timezone")) != NULL)
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"timezone_offset")) != NULL)
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
				}
				/* 匹配子对象2 */
				if((subobject = cJSON_GetObjectItem(object,"now")) != NULL)
				{
					printf("---------------------------------subobject2-------------------------------\r\n");
					if((item = cJSON_GetObjectItem(subobject,"text")) != NULL)//天气预报文字--需要用到的数据
					{
						  printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);

					}
					if((item = cJSON_GetObjectItem(subobject,"code")) != NULL)//天气预报代码--需要用到的数据
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string, item->valuestring);

					}
					if((item = cJSON_GetObjectItem(subobject,"temperature")) != NULL) //温度--需要用到的数据
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string, item->valuestring);
					}
				}
				/* 匹配子对象3 */
				if((subobject = cJSON_GetObjectItem(object,"last_update")) != NULL)
				{
					printf("---------------------------------subobject3-------------------------------\r\n");
					printf("cJSON_GetObjectItem: %s:%s\r\n",subobject->string,subobject->valuestring);
				}
				printf("--------------------------------Now Weather Data End--------------------------------\r\n");
			} 
		
		}
	}
	
	cJSON_Delete(json); //释放cJSON_Parse()分配出来的内存空间


	return 0;

}	


/********************************************************************************************************
** 函数: GetNowWeather, GET 天气实况数据包now.json
**------------------------------------------------------------------------------------------------------
** 参数: void
** 说明: 数据来源：心知天气（api.seniverse.com）
** 返回: 0:获取成功 其他：获取失败	
** 作者: 2017.12.8 by Hezhijie and Lizhengnian 
********************************************************************************************************/
int GetNowWeather(int argc, char *argv[])
{
	char cStr [256] = { 0 };

	if( argc != 2 )
	{	
		printf("Example : GetNowWeather shenzhen");
		return 0;
	}
	
	sprintf ( cStr, "%s%s\r\n\r\n", "GET https://api.seniverse.com/v3/weather/now.json?key=S-KpLoFl1QlBCKOYy&location=", argv[1]);
	printf("%s", cStr);

	ESP8266_Link_Server(enumTCP, (char*)WEATHER_IP, (char*)WEATHER_PORT,Single_ID_0);//TCP
	ESP8266_UnvarnishSend ();//传输模式为：透传	

	macESP8266_Usart ( cStr );//以福州天气为例子获取
	//macESP8266_Usart ( "GET https://api.seniverse.com/v3/weather/now.json?key=S-KpLoFl1QlBCKOYy&location=shenzhen\r\n\r\n");//以福州天气为例子获取
	
	strEsp8266_Fram_Record .InfBit .FramLength = 0;  //重新开始接收新的数据包

	rt_thread_mdelay(1000);
	
	strEsp8266_Fram_Record .Data_RX_BUF [ strEsp8266_Fram_Record .InfBit .FramLength ]  = '\0';
	printf("Receive now data:%s\r\n",strEsp8266_Fram_Record .Data_RX_BUF);
	
	cJSON_NowWeatherParse((char*)strEsp8266_Fram_Record .Data_RX_BUF, &weather_data);	

	ESP8266_ExitUnvarnishSend ( );//退出透传HZJ
  ESP8266_Close_Link (  );//关闭TCP或UDP连接HZJ
	
	return 0;
}
MSH_CMD_EXPORT(GetNowWeather, Get Weather Infomation.);


static int cJSON_WeatherForecastParse(char *JSON, WeatherType *result)
{
	cJSON *json, *arrayItem, *arrayItem1, *object, *subobject, *item;
	u8 i;
	
//printf("Receive now data:%s\r\n",JSON);
 
	json = cJSON_Parse(JSON);//解析JSON数据包
	if(json == NULL)		  //检测JSON数据包是否存在语法上的错误，返回NULL表示数据包无效
	{
		printf("Error before: [%s]\r\n",cJSON_GetErrorPtr()); //打印数据包语法错误的位置
		return 1;
	}
	else
	{
		if((arrayItem = cJSON_GetObjectItem(json,"results")) != NULL); //匹配字符串"results",获取数组内容
		{
			int size = cJSON_GetArraySize(arrayItem);     //获取数组中对象个数
//			printf("cJSON_GetArraySize: size=%d\n",size); 
			
			if((object = cJSON_GetArrayItem(arrayItem,0)) != NULL)//获取数组0的内容
			{
        printf("-------------------------------- Weather Forecast Data Begin------------------------------\r\n");
				/* 匹配子对象1 */
				if((subobject = cJSON_GetObjectItem(object,"location")) != NULL)
				{
					printf("---------------------------------subobject1-------------------------------\r\n");
					if((item = cJSON_GetObjectItem(subobject,"id")) != NULL) //id
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"name")) != NULL) //地名--需要用到的数据
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"country")) != NULL)
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"path")) != NULL)  
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"timezone")) != NULL)
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
					if((item = cJSON_GetObjectItem(subobject,"timezone_offset")) != NULL)
					{
						printf("cJSON_GetObjectItem: %s:%s\r\n",item->string,item->valuestring);
					}
				}
				/* 匹配子对象2 */
				if((subobject = cJSON_GetObjectItem(object,"daily")) != NULL)
				{
					printf("---------------------------------subobject2-------------------------------\r\n");
					
					for(i=0; i<1; i++)
					{
						if((arrayItem1 = cJSON_GetArrayItem(subobject,i)) != NULL)//获取数组0的内容
						{
							if((item = cJSON_GetObjectItem(arrayItem1,"date")) != NULL)//日期
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}
							if((item = cJSON_GetObjectItem(arrayItem1,"text_day")) != NULL)//白天天气
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}
							if((item = cJSON_GetObjectItem(arrayItem1,"text_night")) != NULL)//夜间天气
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}
							if((item = cJSON_GetObjectItem(arrayItem1,"high")) != NULL)//当天最高温度
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}
							if((item = cJSON_GetObjectItem(arrayItem1,"low")) != NULL)//当天最低温度
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}
							if((item = cJSON_GetObjectItem(arrayItem1,"wind_speed")) != NULL)//风速，单位km/h（当unit=c时）、mph（当unit=f时）
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}
							if((item = cJSON_GetObjectItem(arrayItem1,"wind_scale")) != NULL)//风力等级
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}												
							if((item = cJSON_GetObjectItem(arrayItem1,"humidity")) != NULL)//相对湿度，0~100，单位为百分比
							{
								printf("cJSON_GetObjectItem1: %s:%s\r\n",item->string,item->valuestring);
							}								
						}	
					}
				}
				/* 匹配子对象3 */
				if((subobject = cJSON_GetObjectItem(object,"last_update")) != NULL)
				{
					printf("---------------------------------subobject3-------------------------------\r\n");
					printf("cJSON_GetObjectItem: %s:%s\r\n",subobject->string,subobject->valuestring);
				}
				printf("-------------------------------- Weather Forecast Data End--------------------------------\r\n");
			}
		}
	}
	
	cJSON_Delete(json); //释放cJSON_Parse()分配出来的内存空间

	return 0;
}	

int WeatherForecast(void)
{	
	ESP8266_Link_Server(enumTCP, (char*)WEATHER_IP, (char*)WEATHER_PORT,Single_ID_0);//TCP
	ESP8266_UnvarnishSend ();//传输模式为：透传	
	macESP8266_Usart ( "GET https://api.seniverse.com/v3/weather/daily.json?key=S-KpLoFl1QlBCKOYy&location=shenzhen&language=en&unit=c&start=0&days=1\r\n\r\n");
	
	strEsp8266_Fram_Record .InfBit .FramLength = 0;  //重新开始接收新的数据包

	rt_thread_mdelay(1000);
	
	strEsp8266_Fram_Record .Data_RX_BUF [ strEsp8266_Fram_Record .InfBit .FramLength ]  = '\0';
	printf("Receive now data:%s\r\n",strEsp8266_Fram_Record .Data_RX_BUF);
	
	if( strstr(strEsp8266_Fram_Record .Data_RX_BUF, "Rain") || 
			strstr(strEsp8266_Fram_Record .Data_RX_BUF, "Shower") || 
			strstr(strEsp8266_Fram_Record .Data_RX_BUF, "Thunderstorm") || 
			strstr(strEsp8266_Fram_Record .Data_RX_BUF, "Drizzle") )
	{
		vs1053_player_song("0:/music/rain.mp3");	
	}
	
	cJSON_WeatherForecastParse((char*)strEsp8266_Fram_Record .Data_RX_BUF, &weather_data);	

	ESP8266_ExitUnvarnishSend ( );//退出透传HZJ
  ESP8266_Close_Link (  );//关闭TCP或UDP连接HZJ
	
	return 0;
}
MSH_CMD_EXPORT(WeatherForecast, Get Weather Forecast.);

