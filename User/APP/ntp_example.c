#include "config.h"
#include "ntp_example.h"

#define NTP_TIMESTAMP_DELTA 		2208988800ull 																/* number of seconds between 1900 and 1970，1900-1970的时间差 */
#define SEC_TIME_ZONE         	+ (8*60*60)  																	/* Beijing,GMT+8， 时区差 */
															
#define TIME_SERVERIP						"cn.ntp.org.cn"																/* NTP服务器 */
#define TIME_PORTNUM						"123"																					/* NTP服务器端口号，固定为123 */

int GetNtp(void)
{
  uint8_t PosInNtpPacket;
	uint8_t buf[48];																														/* 存储NTP服务器返回的数据 */
	uint32_t local_timestamp;
  ntp_packet packet ;
  struct tm * Net_time; 
  uint8_t NTP_Data[48]; 																											/* 48字节的报文 */																												
	u8 count = 4;
	u8 id;
	
	NTP_Data[0]=0xa3; 																													/* NTP查询报文赋初值 */
	for(PosInNtpPacket=1;PosInNtpPacket<48;PosInNtpPacket++) 
		NTP_Data[PosInNtpPacket]=0;																								/* 剩余的47字节为0 */
	
	rt_kprintf ( "\r\nConfig ESP8266 ......\r\n" );
	ESP8266_Link_Server ( enumUDP, TIME_SERVERIP, TIME_PORTNUM, Single_ID_0 );	/* 连接NTP服务器 */
	ESP8266_UnvarnishSend ();																										/* 开启透传 */
	rt_kprintf ( "\r\nConfig ESP8266 Over\r\n" );
	
	strEsp8266_Fram_Record .InfBit .FramLength = 0;
	rt_memset(strEsp8266_Fram_Record.Data_RX_BUF,0,RX_BUF_MAX_LEN);							/* 清除接收缓存 */
	
	for(PosInNtpPacket=0;PosInNtpPacket<48;PosInNtpPacket++) 										/* 发送NTP查询包 */
	{
		USART_SendData(USART3, (uint8_t)NTP_Data[PosInNtpPacket]);
		while(USART_GetFlagStatus(USART3, USART_FLAG_TC)== RESET);	
	}
	
	rt_thread_mdelay(1000);
	
	rt_memcpy(buf,strEsp8266_Fram_Record.Data_RX_BUF,48);												/* 拷贝数据包 */
	
	printf("/*******************************/\r\n");														/* 接收包原始数据打印 */
	printf("Receive NTP Packet (Hex):");
	for(PosInNtpPacket=0;PosInNtpPacket<48;PosInNtpPacket++)
	{	
		if(PosInNtpPacket%16==0)
		{
			printf("\r\n");
			printf("%02X ",buf[PosInNtpPacket]);
		}
		else
			printf("%02X ",buf[PosInNtpPacket]);
	}
	printf("\r\n/*******************************/\r\n");

	
	ESP8266_ExitUnvarnishSend ( );																							/* 退出透传 */
  ESP8266_Close_Link (  );																										/* 关闭TCP或UDP连接 */
	
	if( packet.txTm_s == 0 )
		return 0;
	
	/* 由于本演示时间精度要求不高，故直接用服务器返回的时间作为对时的时间,并未用公式：时间差offset=（（T2-T1）+（T3-T4））/2。而是用T3作为对时基准时间。*/		
	id = 40;
	packet.txTm_s = buf[id+0]<<24 | buf[id+1]<<16 | buf[id+2]<<8 | buf[id+3];
	
	local_timestamp = packet.txTm_s - NTP_TIMESTAMP_DELTA;											/* 减去1970和1900的差值 */
  local_timestamp +=SEC_TIME_ZONE; 																						/* 加上北京的时间差，GMT+8 */ 
  Net_time = localtime(&local_timestamp); 																		/* 秒数转换位标准时间 */
	
	printf("NTP Time:%04d-%02d-%02d %02d:%02d:%02d\r\n",												/* 打印出时间 */
				(Net_time->tm_year)+1900, (Net_time->tm_mon)+1, 
				 Net_time->tm_mday, Net_time->tm_hour,
				 Net_time->tm_min, Net_time->tm_sec); 

	
	Set_Time(local_timestamp);
	
	while(count--)
	{
		local_timestamp = Get_Time();
		Net_time = localtime(&local_timestamp);
		printf("NTP Time:%04d-%02d-%02d %02d:%02d:%02d\r\n",											/* 打印出时间 */
				(Net_time->tm_year)+1900, (Net_time->tm_mon)+1, 
				 Net_time->tm_mday, Net_time->tm_hour,
				 Net_time->tm_min, Net_time->tm_sec);
		rt_thread_mdelay(1000);
	}
	
	return 0;
}
MSH_CMD_EXPORT(GetNtp, Get BeiJing Time.);


int GetRTCTime(void)
{
	uint32_t local_timestamp; 
  struct tm * Net_time;																												/* 48字节的报文 */																												
	u8 count = 4;
	
	while(count--)
	{
		local_timestamp = Get_Time();
		Net_time = localtime(&local_timestamp);
		printf("NTP Time:%04d-%02d-%02d %02d:%02d:%02d\r\n",											/* 打印出时间 */
				(Net_time->tm_year)+1900, (Net_time->tm_mon)+1, 
				 Net_time->tm_mday, Net_time->tm_hour,
				 Net_time->tm_min, Net_time->tm_sec);
		rt_thread_mdelay(1000);
	}
	
	return 0;
}
MSH_CMD_EXPORT(GetRTCTime, Get RTC Time.);


s_myclock tan_clock;
int Clock(int argc, char *argv[])
{
	if( argc == 2 && strstr(argv[1], "get") )
	{
		rt_interrupt_enter();
		rt_kprintf("getup : %d:%d\n", tan_clock.getup_hour, tan_clock.getup_min);
		rt_kprintf("goout : %d:%d\n", tan_clock.goout_hour, tan_clock.goout_min);
		rt_kprintf("sport : %d:%d\n", tan_clock.sport_hour, tan_clock.sport_min);
		rt_kprintf("sleep : %d:%d\n", tan_clock.sleep_hour, tan_clock.sleep_min);
		rt_kprintf("calorifier : %d:%d\n", tan_clock.calorifier_hour, tan_clock.calorifier_min);
		rt_interrupt_leave();
		return 0;
	}
	
	if( argc != 4 )
	{	
		rt_kprintf("Example : Clock get        \n");
		rt_kprintf("Example : Clock getup 7 30 \n");
		rt_kprintf("Example : Clock goout 8 30 \n");
		rt_kprintf("Example : Clock sport 21 30\n");
		rt_kprintf("Example : Clock sleep 23 30\n");
		rt_kprintf("Example : Clock calorifier 7 00\n");
		return 0;
	}

	if( strstr(argv[1], "getup") )
	{
		rt_interrupt_enter();
		tan_clock.getup_hour = atoi(argv[2]);
		tan_clock.getup_min = atoi(argv[3]);
		rt_interrupt_leave();
	}
	else if( strstr(argv[1], "goout") )
	{
		rt_interrupt_enter();
		tan_clock.goout_hour = atoi(argv[2]);
		tan_clock.goout_min = atoi(argv[3]);
		rt_interrupt_leave();
	}	
	else if( strstr(argv[1], "sport") )
	{
		rt_interrupt_enter();
		tan_clock.sport_hour = atoi(argv[2]);
		tan_clock.sport_min = atoi(argv[3]);
		rt_interrupt_leave();
	}
	else if( strstr(argv[1], "sleep") )
	{
		rt_interrupt_enter();
		tan_clock.sleep_hour = atoi(argv[2]);
		tan_clock.sleep_min = atoi(argv[3]);
		rt_interrupt_leave();
	}
	else if( strstr(argv[1], "calorifier") )
	{
		rt_interrupt_enter();
		tan_clock.calorifier_hour = atoi(argv[2]);
		tan_clock.calorifier_min = atoi(argv[3]);
		rt_interrupt_leave();
	}

	return 0;
}
MSH_CMD_EXPORT(Clock, set or get colck.);
