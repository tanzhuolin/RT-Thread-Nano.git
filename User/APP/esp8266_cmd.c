#include "config.h"
#include "esp8266_cmd.h"

void esp8266_cmd(int argc, char *argv[])
{
	if( !rt_strcmp(argv[1], "AT") )//AT命令测试
	{
		ESP8266_Cmd ( "AT", "OK", NULL, 500 );
		rt_kprintf("%s\r\n",strEsp8266_Fram_Record .Data_RX_BUF);
	}
	else if( !rt_strcmp(argv[1], "RST") )//复位ESP8266
	{	
		ESP8266_Rst();
		//rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}	
	else if( !rt_strcmp(argv[1], "STA") )//设置为工作站模式
	{
		ESP8266_Net_Mode_Choose ( STA );
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}
	else if( !rt_strcmp(argv[1], "JoinAP") )//加入热点：wifi, password
	{
		ESP8266_JoinAP ( argv[2], argv[3]);
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}
	else if(!rt_strcmp(argv[1],"ipconfig")) //查询本机IP
	{
		ESP8266_InquireIP( );
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}	 	
	else if(!rt_strcmp(argv[1],"LinkServer")) //加入服务器 LinkServer,TCP/UDP,ip,port
	{
		if( !rt_strcmp(argv[2],"TCP") )
			ESP8266_Link_Server( enumTCP, argv[3], argv[4], Single_ID_0);
		else
			ESP8266_Link_Server( enumUDP, argv[3], argv[4], Single_ID_0);
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}
	else if( !rt_strcmp(argv[1],"LinkStatus") )
	{
		ESP8266_Cmd ( "AT+CIPSTATUS", "TCP", "UDP", 5000 );
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}
	else if(!rt_strcmp(argv[1],"CloseLink")) //关闭TCP或UDP连接
	{
		ESP8266_Close_Link();
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}
	else if(!rt_strcmp(argv[1],"Unvarnish")) //开启透传
	{
		ESP8266_UnvarnishSend();
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}
	else if(!rt_strcmp(argv[1],"SendData")) //透传时发送数据，数据间不能包含空格；若需发送空格数据请加双引号
	{
		ESP8266_SendString ( ENABLE, argv[2], rt_strlen(argv[2]), Single_ID_0 );
		rt_kprintf("Send Data:%s\r\n",argv[2]);
	}
	else if(!rt_strcmp(argv[1],"ExitUnvarnish")) //关闭透传
	{
		ESP8266_ExitUnvarnishSend ();
		rt_kprintf("%s\n",strEsp8266_Fram_Record.Data_RX_BUF);
	}	
}

MSH_CMD_EXPORT(esp8266_cmd, ESP8266 Test.);


void adc_esp8266_thread_entry(void *parameter)
{
	float ADC_ConvertedValueLocal[NOFCHANEL];	
	
	rt_kprintf ( "\r\nConfig ESP8266 ......\r\n" );
	ESP8266_AT_Test();
	ESP8266_Net_Mode_Choose ( STA );
	while ( ! ESP8266_JoinAP ( macUser_ESP8266_ApSsid, macUser_ESP8266_ApPwd ) );	
	ESP8266_Enable_MultipleId ( DISABLE );
	while ( !	ESP8266_Link_Server ( enumTCP, macUser_ESP8266_TcpServer_IP, macUser_ESP8266_TcpServer_Port, Single_ID_0 ) );
	while ( ! ESP8266_UnvarnishSend () );
	rt_kprintf ( "\r\nConfig ESP8266 Over\r\n" );
	
	while(1)
	{
		rt_interrupt_enter();

		ADC_ConvertedValueLocal[0] = (float)ADC_ConvertedValue[0]/4096*3.3;
		ADC_ConvertedValueLocal[1] = (float)ADC_ConvertedValue[1]/4096*3.3;

		rt_interrupt_leave();
		
		printf("PC1 Value = %f\n", ADC_ConvertedValueLocal[0]);
		printf("PC3 Value = %f\n", ADC_ConvertedValueLocal[1]);
		
		rt_thread_mdelay(2000);
	}
}

void adc_esp8266(void)
{
	rt_thread_t tid;

	tid = rt_thread_create("adc_esp8266_thread",
												 adc_esp8266_thread_entry, RT_NULL,
												 512, 10, 10);

	if (tid != NULL)
			rt_thread_startup(tid);
}

MSH_CMD_EXPORT(adc_esp8266, ADC Test.);
